var gulp = require('gulp');
var sass = require('gulp-sass');
var babel = require('gulp-babel');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var cleanCSS = require('gulp-clean-css');
var htmlreplace = require('gulp-html-replace');
var del = require('del');

var paths = {
  styles: {
    src:  'src/scss/**/*.scss',
    dest: 'src/core/css/'
  },
  scripts: {
    src: 'src/javascript/**/*.js',
    dest: 'src/core/js/',
    js_plugins: 'src/javascript/plugins/**/*.js',
    js_plugins_root: 'src/javascript/plugins/',
    js_vendor: 'src/javascript/vendor/**/*.js',
    js_vendor_dest: 'src/core/js/vendor/**/*.js'
  },
  files: {
    root: 'src/',
    asset_root:'src/core/',
    html: 'src/**/*.html',
    images: 'src/images/**/*.+(png|jpg|jpeg|gif|svg)',
    fonts: 'src/fonts/**/*',
    fonts_root: 'src/core/fonts/',

  },
  build: {
    root:'build/',
    asset_root:'build/core/',
    scripts:'build/core/js/',
    styles:'build/core/css/',
    styles_root: 'core/css/',
    scripts_root: 'core/js/',
    images:'build/images/',
    fonts_root:'build/core/fonts/**/*',

  }
};

/* Not all tasks need to use streams, a gulpfile is just another node program
 * and you can use all packages available on npm, but it must return either a
 * Promise, a Stream or take a callback and call it
 */
function buildClean() {
  // You can use multiple globbing patterns as you would with `gulp.src`,
  // for example if you are using del 2.0 or above, return its promise
  return del(['build/**','!build/images/**']).then(paths => {
    console.log('Deleted files and folders:\n', paths.join('\n'));
});
}

function srcClean() {
  // You can use multiple globbing patterns as you would with `gulp.src`,
  // for example if you are using del 2.0 or above, return its promise
  return del(['src/core/**']).then(paths => {
    console.log('Deleted files and folders:\n', paths.join('\n'));
});
}
/*
 * Define our tasks using plain functions
 */


function srcSass() {
  return gulp.src(paths.styles.src)
  .pipe(sass({errLogToConsole: true}))
  .pipe(sass({
    style: 'expanded',
    sourceComments: 'normal'
  }))
    .pipe(gulp.dest(paths.styles.dest));
}

function srcJs() {
  return gulp.src([
      paths.scripts.src,
      '!' + paths.scripts.js_plugins
    ])
    .pipe(gulp.dest(paths.scripts.dest));
}

function srcCombineJsPlugins() {
  return gulp.src([
      paths.scripts.js_plugins_root],
    { sourcemaps: true })
    .pipe(concat('plugins.js'))
    .pipe(gulp.dest(paths.scripts.dest));
}

function srcCopyFiles() {
  let files = [
      paths.files.fonts,
    ];

    return gulp
      .src(files)
      .pipe(gulp.dest( paths.files.fonts_root ));
}

function watch() {
  gulp.watch(paths.scripts.src, scripts);
  gulp.watch(paths.styles.src, styles);
}

function buildSass() {
  return gulp.src(paths.styles.src)
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(cleanCSS())
    // pass in options to the stream
    .pipe(rename({
      suffix: '.min'
    }))
    .pipe(gulp.dest(paths.build.styles));
}

function buildJs() {
  return gulp.src([
    paths.scripts.src,
    '!' + paths.scripts.js_vendor],
  { sourcemaps: true })
    .pipe(babel())
    .pipe(uglify())
    .pipe(rename({
      suffix: '.min'
    }))
    .pipe(gulp.dest(paths.build.scripts));
}

function buildCombineJsPlugins() {
  return gulp.src([
    paths.scripts.src,
    '!' + paths.scripts.js_vendor],
  { sourcemaps: true })
    .pipe(babel())
    .pipe(uglify())
    .pipe(concat('plugins.min.js'))
    .pipe(gulp.dest(paths.build.scripts));
}

function buildHTML() {
  return gulp.src(paths.files.html,)
  .pipe(htmlreplace({
        'css': {
          src: [['main']],
          tpl: '<link rel="stylesheet" href="' + paths.build.styles_root + '%s.min.css">'
        },
        'js': {
          src: [['plugins', 'main']],
          tpl: '<script type ="text/javascript" src="' + paths.build.scripts_root + '%s.min.js"></script>' +
               '<script type ="text/javascript" src="' + paths.build.scripts_root + '%s.min.js"></script>'
        },
    }))
    .pipe(gulp.dest(paths.build.root));
}
function buildCopyFiles() {
  let files = [
      paths.scripts.js_vendor_dest,
      paths.files.images,
      paths.files.fonts_root + '**/*',
    ];
    return gulp
      .src(files, {base: paths.files.root})
      .pipe(gulp.dest(paths.build.root ));
}

/*
 * Specify if tasks run in series or parallel using `gulp.series` and `gulp.parallel`
 */
var build = gulp.series(buildClean, gulp.parallel(buildHTML, buildSass, buildJs, buildCombineJsPlugins, buildCopyFiles));

var defaulttask = gulp.series(srcClean, gulp.parallel(srcSass, srcJs, srcCombineJsPlugins, srcCopyFiles));


/*
 * You can use CommonJS `exports` module notation to declare tasks
 */
exports.clean = buildClean;
exports.sass = srcSass;
exports.buildsass = buildSass;
exports.buildjs = buildJs;
exports.scripts = srcJs;
exports.watch = watch;
exports.build = build;
/*
 * Define default task that can be called by just running `gulp` from cli
 */
exports.default = defaulttask;
