# Site Template

This is a site template based on [skeleton](http://getskeleton.com/) and uses gulp to build the site


## Instructions 
1) Clone the repo 
2) Edit `package.json`
3) Run `npm install`
4) Run `gulp` or `gulp build` to compile the site.